cmake_minimum_required(VERSION 2.8.3)
project(um_acc)

# add c++11
set(CMAKE_CXX_FLAGS "-std=c++0x ${CMAKE_CXX_FLAGS}")

# Depenencies
find_package(catkin REQUIRED COMPONENTS roscpp rosconsole std_msgs message_generation eiquadprog)

# Services and msgs
add_message_files(DIRECTORY msg FILES ControlSignal.msg CarState.msg Pwm.msg CarSignal.msg CarStateExtended.msg)
add_service_files(DIRECTORY srv FILES ComputeControl.srv)
generate_messages(DEPENDENCIES std_msgs um_acc)

# Declare catking package
catkin_package(CATKIN_DEPENDS roscpp std_msgs message_runtime)

# Declare and build targets
add_library(qphelper STATIC src/qphelper.cpp)
add_library(kalman STATIC src/kalman.cpp)

include_directories(cudd-2.5.0/include ${catkin_INCLUDE_DIRS})

add_definitions(-lstdc++ -lm)

add_executable(compute_control_um src/compute_control_um.cpp)
add_dependencies(compute_control_um um_acc_generate_messages_cpp)
target_link_libraries(compute_control_um ${catkin_LIBRARIES} qphelper)
######

set(cudd_LIBRARIES_REL cudd dddmp epd util mtr st)
set(cudd_LIBRARIES)
foreach(lib ${cudd_LIBRARIES_REL})
  list(APPEND cudd_LIBRARIES ${CMAKE_CURRENT_SOURCE_DIR}/cudd-2.5.0/${lib}/lib${lib}.a)
endforeach()
#message("libs: ${cudd_LIBRARIES}")

#target_link_libraries(
 #compute_control_ucla
 #  ${catkin_LIBRARIES} 
 #  ${cudd_LIBRARIES}
 #)


#####

add_executable(compute_control_ucla src/compute_control_ucla.cpp)
add_dependencies(compute_control_ucla um_acc_generate_messages_cpp)
target_link_libraries(compute_control_ucla ${cudd_LIBRARIES} ${catkin_LIBRARIES} qphelper)

add_executable(car_simulator src/car_simulator.cpp)
add_dependencies(car_simulator um_acc_generate_messages_cpp)
target_link_libraries(car_simulator ${catkin_LIBRARIES})

#add_executable(rc_interface_ucla src/rc_interface_ucla.cpp)
#add_dependencies(rc_interface_ucla um_acc_generate_messages_cpp)
#target_link_libraries(rc_interface_ucla ${cudd_LIBRARIES} ${catkin_LIBRARIES} kalman)

add_executable(rc_interface src/rc_interface.cpp)
add_dependencies(rc_interface um_acc_generate_messages_cpp)
target_link_libraries(rc_interface ${cudd_LIBRARIES} ${catkin_LIBRARIES} kalman)

add_executable(controller src/controller.cpp)
add_dependencies(controller um_acc_generate_messages_cpp)
target_link_libraries(controller ${catkin_LIBRARIES})

add_executable(teleop_car src/teleop_car.cpp)
add_dependencies(teleop_car um_acc_generate_messages_cpp)
target_link_libraries(teleop_car ${catkin_LIBRARIES})

## Specify libraries to link a library or executable target against



